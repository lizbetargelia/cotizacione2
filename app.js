const http = require("http");
const express = require("express");
const { dirname } = require("path");
const app = express();
const bodyparser = require("body-parser");



let datos = [{
    matricula:"2020030322",
    nombre: "Acosta Ortega Jesus Humberto",
    sexo: 'M',
    materias:["Ingles","Tecnologias I", " Base de Datos"]

},

{
    matricula:"2020030325",
    nombre: "Acosta Varela Irving Guadalupe",
    sexo: 'M',
    materias:["Ingles","Matematicas", " Base de Datos"]
},
{
    
    matricula:"2020030324",
    nombre: "Juarez Lugo Victor Uriel",
    sexo: 'M',
    materias:["Ingles","Tecnologias I", " Español"]
    
},
{
    matricula:"2020030300",
    nombre: "Rios Muñoz Brian Eduardo",
    sexo: 'M',
    materias:["Ingles","Química", " Base de Datos"]
},
{
    matricula:"2020030323",
    nombre: "Hernandez Colio Mariana de Jesus",
    sexo: 'F',
    materias:["Ingles","Matematicas", " Base de Datos"]
},
{
    matricula:"2020030345",
    nombre: "Santoyo Pella Gabriel",
    sexo: 'M',
    materias:["Ingles","Tecnologias I", " Base de Datos"]
}
]

app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));

app.get('/', (req, res)=>{

    res.render('index',{titulo:"Listado de alumnos", listado:datos})
    //res.send("<h1> Iniciamos con express <h1>");
    
    
});

app.get("/tablas",(req,res)=>{
    const valores = {
        tabla:req.query.tabla
    }
    res.render('tablas',valores);

});

app.post("/tablas",(req,res)=>{
    const valores = {
        tabla:req.body.tabla
    }
    res.render('tablas',valores);

});
//cotizacion
app.get("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
        

    }
    res.render('cotizacion',valores);
});

app.post("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.body.valor,
        plazos:req.body.plazos,
        pInicial:req.body.pInicial
        
    }
    res.render('cotizacion',valores);
});



app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');

});
//escuchar el servidor por el puerto 3000
const puerto = 3002;
app.listen(puerto, ()=>{

    console.log("Iniciado puerto 3002");
});
